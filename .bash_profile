export JAVA_HOME=/Library/Java/JavaVirtualMachines/jdk1.8.0_221.jdk/Contents/Home
export NVM_DIR=~/.nvm
export PATH=/usr/local/bin:$PATH
source $(brew --prefix nvm)/nvm.sh
export PATH=/Users/abbasq6v/Documents/Selenium-drivers:$PATH

alias i='npm install'
alias un='npm un'
alias id='npm install --save-dev'
alias ig='npm install --global'
alias t='npm test'
alias r='npm run'
alias aw='aws-azure-login --mode gui'
alias s3ls='aws s3 ls ss-wcms-iapi-primary-bucket-preview-develop'
alias mx='mobx-devtools'

alias jm='open /usr/local/bin/jmeter'

eval 'keychain --eval --agents ssh --inherit any id_rsa'

export PATH=$PATH:/Applications/Postgres.app/Contents/Versions/latest/bin

export WEBIDE_PROPERTIES=~/.idea_cache

export PATH="/usr/local/opt/openssl/bin:$PATH"
export PATH="/Users/abbasq6v/Documents/dev2/flutter/bin:$PATH"
export PATH="/Users/abbasq6v/.nvm/versions/node/v8.16.0/bin/node:$PATH"
export LDFLAGS="-L/usr/local/opt/openssl/lib"
export CPPFLAGS="-I/usr/local/opt/openssl/include"
export PKG_CONFIG_PATH="/usr/local/opt/openssl/lib/pkgconfig"

alias m5='openssl md5 -binary $(pbpaste) | base64'

# >>> conda initialize >>>
# !! Contents within this block are managed by 'conda init' !!
__conda_setup="$('/Users/abbasq6v/anaconda3/bin/conda' 'shell.bash' 'hook' 2> /dev/null)"
if [ $? -eq 0 ]; then
    eval "$__conda_setup"
else
    if [ -f "/Users/abbasq6v/anaconda3/etc/profile.d/conda.sh" ]; then
        . "/Users/abbasq6v/anaconda3/etc/profile.d/conda.sh"
    else
        export PATH="/Users/abbasq6v/anaconda3/bin:$PATH"
    fi
fi
unset __conda_setup
# <<< conda initialize <<<

