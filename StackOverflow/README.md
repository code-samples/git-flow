```
<!-- begin snippet: js hide: false console: true babel: false -->

<!-- language: lang-js -->

    let sampleData = Array.from({length: 10}, (_, i) => ({id:i}))

    console.log(sampleData)

<!-- end snippet -->
```

# My answers

[Filter/Search JSON Data](https://stackoverflow.com/a/51529907/699091)
